<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq3_Block_Adminhtml_Faq_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    /**
     * @todo set block group
     */
    protected $_blockGroup = 'absoluteweb_faq3/faq';

    public function __construct()
    {
        parent::__construct();

        /**
         * @todo set controller, then update buttons save & delete
         */
        $this->_objectId = 'id';
        $this->_blockGroup = 'absoluteweb_faq3';
        $this->_controller = 'Adminhtml_Faq';
        $this->_mode = 'edit';
        $this->_updateButton('save', 'label', Mage::helper('absoluteweb_faq3')->__('Save question'));
        $this->_updateButton('delete', 'label', Mage::helper('absoluteweb_faq3')->__('Delete question'));

        if (!$this->_getModel()->getId()) {
            /**
             *
             * @todo remove button delete
             */
            $this->_removeButton('delete');
        }
    }

    public function getHeaderText()
    {
        /**
         * @todo if register model is set, return 'Edit Faq', else return 'Add new Faq' by using helper
         */
        if ($this->_getModel() && $this->_getModel()->getId()) {
            return Mage::helper('absoluteweb_faq3')->__('Edit question',  $this->htmlEscape($this->_getModel()->getTitle()));
        }

        return Mage::helper('absoluteweb_faq3')->__('Add new question');
    }


    public function getHeaderCssClass()
    {
        return 'icon-head head-faq';
    }

    private function _getModel()
    {
        $model = Mage::registry('current_faq');
        if (!$model || !($model instanceof AbsoluteWeb_Faq3_Model_Faq)) {
            Mage::throwException('Model is not good');
        }

        return $model;
    }
}