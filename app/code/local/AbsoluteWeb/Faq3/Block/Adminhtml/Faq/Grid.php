<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq3_Block_Adminhtml_Faq_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        /**
         * @todo set grid id, sort and save to session parameters
         */
        $this->setDefaultSort('id');
        $this->setId('absoluteweb_faq3_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/new', array
        (
            'id' => $row->getEntityId()
        ));
    }

    protected function _prepareCollection()
    {
        /**
         * @todo init faq collection
         */
        $collection = $this->_getModel()->getCollection();
        $collection = Mage::registry('current_faq')->getCollection();
        $collection->getSelect()->join('admin_user', 'main_table.user_id = admin_user.user_id ', 'admin_user.username');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Configuration of grid
     */
    protected function _prepareColumns()
    {
        /**
         * @todo prepare columns for grid
         */
        $this->addColumn('entity_id',
            array(
                'header' => Mage::helper('absoluteweb_faq3')->__('Id'),
                'align' => 'right',
                'width' => '50px',
                'index' => 'entity_id',
                'sortable' => true
            )
        );
        $this->addColumn('question',
            array(
                'header' => Mage::helper('absoluteweb_faq3')->__('question'),
                'index' => 'question'
            )
        );
        $this->addColumn('answer',
            array(
                'header' => Mage::helper('absoluteweb_faq3')->__('answer'),
                'index' => 'answer'
            )
        );
        $this->addColumn('username',
            array(
                'header' => Mage::helper('absoluteweb_faq3')->__('admin'),
                'index' => 'username'
            )
        );
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('deleteMass', array(
            'label' => Mage::helper('absoluteweb_faq3')->__('delete some'),
            'url' => $this->getUrl('*/*/deleteMass'),
            'confirm' => Mage::helper('absoluteweb_faq3')->__('Are you sure?')
        ));

        return $this;
    }

    private function _getModel()
    {
        $model = Mage::registry('current_faq');
        if (!$model || !($model instanceof AbsoluteWeb_Faq3_Model_Faq)) {
            Mage::throwException('Model is not good');
        }

        return $model;
    }

}