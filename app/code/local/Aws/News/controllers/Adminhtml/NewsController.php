<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class Aws_News_Adminhtml_NewsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * News grid
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('aws_news');
        $this->renderLayout();
    }

    /**
     * The method creates new news.
     */
    public function newAction()
    {
        $this->_initNews();
        $this->loadLayout();
        $this->_setActiveMenu('aws_news');

        $this->_addContent(
            $this->getLayout()->createBlock('aws_news/adminhtml_news_edit', 'edit'),
            $this->getLayout()->createBlock('aws_news/adminhtml_news_edit_form', 'new')
        );
        $this->renderLayout();
    }

    /**
     * The method edits the current news
     */
    public function editAction()
    {
        $this->_forward('new');
    }

    /**
     * The method saves an edit or new action
     */
    public function saveAction()
    {
        /** @var Aws_News_Model_News $newsObject */
        $newsObject = Mage::getModel('aws_news/news');
        $id = $this->getRequest()->getParam('news_id', null);
        $data = $this->getRequest()->getParams();
        if ((!intval($id)) && ($id != null)) {
            $this->_setErrorMessage('invalid id');
            $this->_redirect('*/*/index');
            return;
        }

        try {
            if ($id) {
                $newsObject->load($id);
            }
            $newsObject->addData($data)->save();
            $msg = $this->__('The item had been edited');

            if ($newsObject->isObjectNew()) {
                $msg = $this->__('The new item had been added');
            }
            $this->_setSuccessMessage($msg);
            $this->_redirect('*/*/index');
        } catch (Mage_Core_Exception $e) {
            $this->_setErrorMessage($e->getMessage(                                                                                        ));
            $this->_redirect('*/*/index');

        } catch (Exception $e) {
            $this->_setErrorMessage("Something went wrong");
            Mage::log($e->getMessage(), 1);
            $this->_redirect('*/*/index');
        }
    }

    /**
     * This method deletes the array of news
     */
    public function deleteMassAction()
    {
        $requestId = $this->getRequest()->getParam('id');

        if (!is_array($requestId)) {
            $this->_setErrorMessage('Is not array');
            $this->_redirect('*/*/index');
            return;
        }

        try {
            foreach ($requestId as $id) {
                $this->_currentDelete($id);
            }
            $this->_setSuccessMessage('Total of record(s) were successfully deleted');

        } catch (Exception $e) {
            $this->_setErrorMessage("something went wrong");
            Mage::log($e->getMessage(), 1);
            $this->_redirect('*/*/index');
        }
        $this->_redirect('*/*/index');
    }

    /**
     * This method delete the news
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            if ((!intval($id)) && ($id != null)) {
                $this->_setErrorMessage('invalid id');
                $this->_redirect('*/*/index');
                return;
            }

            $this->_currentDelete($id);
            Mage::getSingleton('core/session');
            $this->_setSuccessMessage('The item had been deleted');
            $this->_redirect('*/*/index');

        } catch (Exception $e) {
            $this->_setErrorMessage($e->getMessage());
        }
    }

    /**
     * Init actions
     */
    protected function _initNews()
    {
        $helper = Mage::helper('aws_news');
        $this->_title($helper->__('Aws_News'))->_title($helper->__('NEWS'));

        $model = Mage::getModel('aws_news/news');
        $newsId = $this->getRequest()->getParam('id');

        if (!is_null($newsId)) {
            $model->load($newsId);
        }
        Mage::register('current_news', $model);
    }

    /**
     * @id - identificator of the current news
     */
    private function _currentDelete($id)
    {
        $model = Mage::getModel('aws_news/news')->load($id);
        if (!$model || !($model instanceof Aws_News_Model_News)) {

            $this->_redirect('*/*/index');
            Mage::throwException('Model is not good');
        }
        $model->delete();
    }

    /**
     * @msg - the text of message
     */
    private function _setSuccessMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addSuccess($this->__($msg));
    }

    /**
     * @msg - the text of message
     */
    private function _setErrorMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addError($this->__($msg));
    }
}