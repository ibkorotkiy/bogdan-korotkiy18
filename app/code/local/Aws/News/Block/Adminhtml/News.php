<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class Aws_News_Block_Adminhtml_News extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * this is handle to block that set in config
     */
    protected $_blockGroup = 'aws_news';

    protected function _construct()
    {
        /**
         * this is path to grid block
         */
        $this->_controller = 'adminhtml_news';
        $this->_blockGroup = 'aws_news';
        $this->_headerText = 'News';
        $this->setUseAjax(true);
        $this->_addButtonLabel = $this->__('Add new news');
        parent::_construct();
    }

    /**
     * Redefine header css class
     *
     * @return string
     */
    public function getHeaderCssClass()
    {
        return 'icon-head head-faq';
    }
}