<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class Aws_News_Block_Adminhtml_News_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _construct()
    {
        parent::_construct();
        $this->setDefaultSort('id');
        $this->setId('aws_news_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array
        (
            'id' => $row->getEntityId()
        ));
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('aws_news/news')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * Configuration of grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header' => Mage::helper('aws_news')->__('Id'),
                'align' => 'right',
                'width' => '50px',
                'index' => 'entity_id',
                'sortable' => true,
                'type' => 'number'
            )
        );

        $this->addColumn('title',
            array(
                'header' => Mage::helper('aws_news')->__('Title'),
                'index' => 'title',
            )
        );

        $this->addColumn('content',
            $html = array(
                'header' => Mage::helper('aws_news')->__('Content'),
                'index' => 'content',
                'type' => 'text',
                'string_limit' => '255',
                'renderer' => 'Aws_News_Block_Adminhtml_Grid_Column_Renderer_Striptext',
            )
        );

        $this->addColumn('create_date',
            array(
                'header' => Mage::helper('aws_news')->__('Create_Date'),
                'index' => 'create_date',
                'type' => 'datetime',
            )
        );

        $this->addColumn('update_date',
            array(
                'header' => Mage::helper('aws_news')->__('Update Date'),
                'index' => 'update_date',
                'type' => 'datetime',
            )
        );

        $yesNo = Mage::getSingleton('adminhtml/system_config_source_yesno');
        $this->addColumn('publish',
            array(
                'header' => Mage::helper('aws_news')->__('Publish'),
                'index' => 'publish',
                'type' => 'options',
                'options' => $yesNo->toArray(),
            )
        );

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('deleteMass', array(
            'label' => Mage::helper('aws_news')->__('delete some'),
            'url' => $this->getUrl('*/*/deleteMass'),
            'confirm' => Mage::helper('aws_news')->__('Are you sure?')
        ));
        return $this;
    }
}