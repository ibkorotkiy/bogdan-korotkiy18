<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq2_IndexController extends Mage_Core_Controller_Front_Action
{


    /**
     * This method is output all questions and answers to them
     * For example you may visit the following URL http://example.com/frontName/index/getAllFaq
     */
    public function getAllFaqAction()
    {
        $modelAll = Mage::getResourceModel('absoluteweb_faq2/faq_collection');

        Mage::register('all_faq_model', $modelAll);
        $this->_initLayout();
    }

    /**
     * Render form to add new faq
     */
    public function addNewFaqAction()
    {
        $this->_forward('editFaqById');

    }

    /**
     * Render form to edit faq
     */
    public function editFaqByIdAction()
    {
        $id = $this->getRequest()->getParam('id', null);

        if(!$id){
            Mage::getModel('absoluteweb_faq2/faq');
            $this->_initLayout();
        }

        $model = Mage::getModel('absoluteweb_faq2/faq')
            ->load($id);


        if(!$model->getId()) {
            $this->_getErrorMessage('invalid id');

            $this->_initLayout();
            return;
        }

        Mage::register('aws_faq_model', $model);
        $this->_initLayout();
    }

    /**
     * Save faq by using id or add new record
     */
    public function saveAction()
    {
        $faqObject = Mage::getModel('absoluteweb_faq2/faq');
        $id = $this->getRequest()->getParam('question_id');
        $arr = $this->getRequest()->getParams();

        if ((!intval($id)) && ($id!=null)){
            $this->_getErrorMessage('invalid id');
            $this->_redirect('Kor/index/getAllFaq');
            return;
            /** @var Mage_Core_Model_Session $session */
        }

        if ($id){
            $faqObject = $faqObject->load($id);
        }

        $faqObject->addData($arr)
            ->save();

        $msg = $this->__('The item had been edited');

        if ($faqObject->isObjectNew()){
            $msg = $this->__('The new item had been added');
        }

        $this->_getSuccessMessage($msg);

        $this->_redirect('Kor/index/getAllFaq');
    }

    /**
     * @todo here you must realize create or edit logic
     *      if id exist in post, you. can load model by using it otherwise create new record
     *      add message about successful saving or editing by using session (see models such as Mage_Core_Model_Session and extended classes)
     */

    /**
     * Delete faq by id
     */

    public function deleteAction()
    {
        /**
         * @todo get id sent by url and delete faq
         *       add message by using session
         */

        $id = $this->getRequest()->getParam('id');

        Mage::getModel('absoluteweb_faq2/faq')->load($id)->delete();
        $session = Mage::getSingleton('core/session');
        $session->addSuccess('The item had been deleted');
        $this->_redirect('Kor/index/getAllFaq');
    }

    private function _initLayout()
    {
        $this->loadLayout()->renderLayout();
    }

    private function _getSuccessMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addSuccess($this->__($msg));
    }

    private function _getErrorMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addError($this->__($msg));
    }
}