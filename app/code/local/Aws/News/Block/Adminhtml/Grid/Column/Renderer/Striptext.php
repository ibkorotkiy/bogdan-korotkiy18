<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class Aws_News_Block_Adminhtml_Grid_Column_Renderer_Striptext extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Longtext
{
    public function render(Varien_Object $row)
    {
        $result = parent::render($row);

        return $this->stripTags($result);
    }
}