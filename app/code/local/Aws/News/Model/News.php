<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class Aws_News_Model_News extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        parent::_construct();
        $this->_init('aws_news/news');
    }

    protected function updateDate()
    {
        $data = array(
            'update_date' => Mage::getSingleton('core/date')->gmtDate()
        );
        return $this->addData($data);
    }

    protected function createDate()
    {
        $data = array(
            'create_date' => Mage::getSingleton('core/date')->gmtDate()
        );

        $this->addData($data);
        return;
    }

    protected function _beforeSave()
    {
        if (!Zend_Validate::is($this->getTitle(), 'NotEmpty')) {
            Mage::throwException(Mage::helper('aws_news')->__('Title can\'t be empty'));
        }

        if (!Zend_Validate::is($this->getImage(), 'NotEmpty')) {
            Mage::throwException(Mage::helper('aws_news')->__('Image can\'t be empty'));
        }

        if (!Zend_Validate::is($this->getContent(), 'NotEmpty')) {
            Mage::throwException(Mage::helper('aws_news')->__('Content can\'t be empty'));
        }

        if (!Zend_Validate::is($this->getPublish(), 'NotEmpty')) {
            Mage::throwException(Mage::helper('aws_news')->__('Publication sample can\'t be empty'));
        }

        if ($this->isObjectNew()) {
            $this->createDate();
        }
        $this->updateDate();

        parent::_beforeSave();
    }
}
