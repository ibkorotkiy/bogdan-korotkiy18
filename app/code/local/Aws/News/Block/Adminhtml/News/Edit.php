<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class Aws_News_Block_Adminhtml_News_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected $_blockGroup = 'aws_news/news';

    protected function _construct()
    {
        parent::_construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'aws_news';
        $this->_controller = 'adminhtml_news';
        $this->_mode = 'edit';
        $this->_updateButton('save', 'label', Mage::helper('aws_news')->__('Save news'));
        $this->_updateButton('delete', 'label', Mage::helper('aws_news')->__('Delete news'));

        if (!$this->_getModel()->getId()) {
            $this->_removeButton('delete');
        }
    }

    public function getHeaderText()
    {
        if ($this->_getModel() && $this->_getModel()->getId()) {
            return Mage::helper('aws_news')->__("Edit news",
                $this->htmlEscape($this->_getModel()->getTitle()));
        }
        return Mage::helper('aws_news')->__('Add new news');
    }

    public function getHeaderCssClass()
    {
        return 'icon-head head-news';
    }

    private function _getModel()
    {
        $model = Mage::registry('current_news');
        if (!$model || !($model instanceof Aws_News_Model_News)) {
            Mage::throwException('Model is not good');
        }
        return $model;
    }
}