<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq3_Adminhtml_FaqController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Faq grid
     */
    public function indexAction()
    {

        $this->_initFaq();
        $this->loadLayout();

        /**
         * @todo: set active menu
         */
        $this->_setActiveMenu('absoluteweb_faq3');
        $this->renderLayout();

    }

    /**
     * The method creates new item.
     */
    public function newAction()
    {
        $this->_initFaq();
        $this->loadLayout();
        /**
         * @todo: set active menu end aad
         */
        $this->_setActiveMenu('absoluteweb_faq3');

        /**
         * @todo: append faq block (absoluteweb_faq3/adminhtml_faq_edit) to content
         *      (you may find in magento how do it, for example go to Mage -> Adminhtml -> controllers and open CustomerController)
         */
        $this->_addContent(
            $this->getLayout()->createBlock('absoluteweb_faq3/adminhtml_faq_edit', 'edit'),
            $this->getLayout()->createBlock('absoluteweb_faq3/adminhtml_faq_edit_form', 'new')
        );

        $this->renderLayout();
    }

    /**
     * The method edits the current faq
     */
    public function editAction()
    {
        $this->_forward('new');
    }

    /**
     * The method saves an edit or new action
     */
    public function saveAction()
    {
        /**
         * @todo save to db all params from post
         *      to 'user_id' field set id of the current administrator
         */
        $adminId = Mage::getSingleton('admin/session')->getUser()->getUserId();
        $faqObject = Mage::getModel('absoluteweb_faq3/faq');
        $id = $this->getRequest()->getParam('question_id', null);

        if ((!intval($id)) && ($id != null)) {
            $this->_setErrorMessage('invalid id');
            $this->_redirect('*/*/index');
            return;
        }

        $data = array(
            'question' => $this->getRequest()->getParam('question', ''),
            'answer' => $this->getRequest()->getParam('answer', ''),
            'user_id' => $adminId
        );

        if ($id) {
            $faqObject = $faqObject->load($id);
        }

        $faqObject->addData($data)
            ->save();
        $msg = $this->__('The item had been edited');

        if ($faqObject->isObjectNew()) {
            $msg = $this->__('The new item had been added');
        }
        $this->_setSuccessMessage($msg);
        $this->_redirect('*/*/index');
    }

    /**
     * This method deletes the array of items
     */
    public function deleteMassAction()
    {
        /**
         * @todo delete faq by id, use try..catch blocks,
         *          set message about delete process to session and then set redirect to index action
         */
        $requestId = $this->getRequest()->getParam('id');
        try {
            if (!is_array($requestId)) {
                Mage::throwException('Please select reqeust(s)');

            }
            foreach ($requestId as $id) {
                $this->_currentDelete($id);
            }
            $this->_setSuccessMessage('Total of record(s) were successfully deleted');

        } catch (Exception $e) {
            $this->_setErrorMessage($e->getMessage());
        }

        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            if ((!intval($id)) && ($id != null)) {
                $this->_setErrorMessage('invalid id');
                $this->_redirect('*/*/index');
                return;
            }

            $this->_currentDelete($id);
            Mage::getSingleton('core/session');
            $this->_setSuccessMessage('The item had been deleted');
            $this->_redirect('*/*/index');

        } catch (Exception $e) {
            $this->_setErrorMessage($e->getMessage());
        }
    }

    /**
     * Init actions
     *
     * @return AbsoluteWeb_Faq3_Model_Faq
     */
    protected function _initFaq()
    {
        $helper = Mage::helper('absoluteweb_faq3'); // @todo: you must set properly URI of your helper
        $this->_title($helper->__('AbsoluteWeb'))->_title($helper->__('FAQ'));

        /**
         * you can see about register method there http://alanstorm.com/magento_registry_singleton_tutorial
         */
        $model = Mage::getModel('absoluteweb_faq3/faq');
        $faqId = $this->getRequest()->getParam('id');

        if (!is_null($faqId)) {
            $model->load($faqId);
        }
        Mage::register('current_faq', $model);

    }

    /**
     * @id - identificator of the current item
     */
    private function _currentDelete($id)
    {
        $model = Mage::getModel('absoluteweb_faq3/faq')->load($id);
        if (!$model || !($model instanceof AbsoluteWeb_Faq3_Model_Faq)) {

            $this->_redirect('*/*/index');
            Mage::throwException('Model is not good');
        }

        $model->delete();
    }

    /**
     * @msg - the text of message
     */
    private function _setSuccessMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addSuccess($this->__($msg));
    }

    /**
     * @msg - the text of message
     */
    private function _setErrorMessage($msg)
    {
        return Mage::getSingleton('core/session')
            ->addError($this->__($msg));
    }

}