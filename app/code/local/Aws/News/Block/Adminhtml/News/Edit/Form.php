<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class Aws_News_Block_Adminhtml_News_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * Prepare form for render
     */
    protected $_blockGroup = 'aws_news/news';

    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }

        $helper = Mage::helper('aws_news');
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getUrl('*/*/save'),
            'method' => 'post'
        ));

        $news = $this->_getModel();
        $form->setUseContainer(true);
        $this->setForm($form);
        $fieldset = $form->addFieldset('base_fieldset', array('legend' => $helper->__('Create new news')));

        if ($news->getId()) {
            $fieldset->addField('news_id', 'hidden', array(
                'name' => 'news_id',
                'value' => $news->getId()
            ));
        }

        $fieldset->addField('title', 'text', array(
            'label' => Mage::helper('aws_news')->__('Title'),
            'class' => 'required-entry',
            'required' => false,
            'name' => 'title'
        ));

        $fieldset->addField('image', 'text', array(
            'label' => Mage::helper('aws_news')->__('Image'),
            'class' => 'required-entry',
            'required' => true,
        ));

        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config');
        $fieldset->addField('content', 'editor', array(
            'label' => Mage::helper('aws_news')->__('Content'),
            'class' => 'required-entry',
            'name' => 'content',
            'required' => true,
            'wysiwyg' => true,
            'config' => $wysiwygConfig
        ));

        /** @var $sourceYesNo Mage_Adminhtml_Model_System_Config_Source_Yesno */
        $yesNo = Mage::getSingleton('adminhtml/system_config_source_yesno');
        $fieldset->addField('publish', 'select', array(
            'label' => Mage::helper('aws_news')->__('Publish'),
            'title' => Mage::helper('eav')->__('Default Value'),
            'class' => 'required-entry',
            'required' => true,
            'default' => '0',
            'name' => 'publish',
            'options' => $yesNo->toArray(),
        ));

        $form->addValues($news->getData());
    }

    private function _getModel()
    {
        $model = Mage::registry('current_news');
        if (!$model || !($model instanceof Aws_News_Model_News)) {
            Mage::throwException('Model is not good');
        }
        return $model;
    }
}