<?php
$installer = $this;
$installer->startSetup();

/**
 * @todo add table absoluteweb_faq3 (don't use method run(), it's deprecated method)
 *
 * entity_id    - int, not null, auto increment, primary key
 * question     - varchar
 * answer       - varchar
 * user_id      - int (current admin ID), set foreign key for this field (related with table - 'admin_user', field - 'user_id')
 */

$table = $installer->getConnection()
    ->newTable($installer->getTable('absoluteweb_faq3/faq'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
    ), 'Entity ID')
    ->addColumn('question', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false,
    ), 'AbsoluteWeb_Faq3 question')
    ->addColumn('answer', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false,
    ), 'AbsoluteWeb_Faq3 answer')
    ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null)
    ->addForeignKey('user_id', 'user_id', 'admin_user', 'user_id');
$installer->getConnection()->createTable($table);
$installer->endSetup();