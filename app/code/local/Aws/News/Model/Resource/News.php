<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class Aws_News_Model_Resource_News extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Set main entity table name and primary key field name
     */
    protected function _construct()
    {
        $this->_init('aws_news/news', 'entity_id');
    }
}