<?php
/**
 * Absolute Web Intellectual Property
 *
 * @copyright    Copyright © 1999-2019 Absolute Web, Inc. (http://www.absoluteweb.com)
 * @author       Absolute Web
 * @license      http://www.absoluteweb.com/license-agreement/  Single domain license
 * @terms of use http://www.absoluteweb.com/terms-of-use/
 */

class AbsoluteWeb_Faq3_Block_Adminhtml_Faq extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * this is handle to block that set in config
     */

    protected $_blockGroup = 'absoluteweb_faq3';

    public function __construct()
    {
        /**
         * this is path to grid block
         */
        $this->_controller = 'adminhtml_faq';
        /**
         * @todo modify header & button labels by using using helper
         */
        $this->_blockGroup = 'absoluteweb_faq3';
        $this->_headerText = 'GRID';
        $this->_addButtonLabel = $this->__('Add new item');
        parent::__construct();
    }
    /**
     * Redefine header css class
     *
     * @return string
     */
    public function getHeaderCssClass()
    {
        return 'icon-head head-faq';
    }

}